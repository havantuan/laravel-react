import permissions from "../permissions/permissions";

const sideBarMenuConfig = {
  accountant: {
    group_00: {
      title: 'Sự kiện',
      key: 'group_00',
      icon: 'schedule',
      url: ''
    },
    // group_1: {
    //   title: 'Tài liệu',
    //   key: 'group_1',
    //   icon: 'file-text',
    //   url: '/document'
    // },
    group_2: {
      title: 'Kênh thông báo nhanh',
      key: 'group_2',
      icon: 'notification',
      url: '/notification'
    },
    group_3: {
      title: 'Người dùng',
      key: 'group_3',
      icon: 'user',
      url: '/users',
      permission: permissions.readUser
    },
    post: {
      title: 'Bài viết',
      key: 'Post',
      icon: 'file-text',
      url: '/post'
    },
    group_5: {
      title: 'Vai trò',
      key: 'group_5',
      icon: 'tags-o',
      url: '/role',
      permission: permissions.readRole
    },
    group_11: {
      title: 'Giải thưởng',
      key: 'group_11',
      icon: 'gift',
      url: '/award'
    },
    group_13: {
      title: 'Tin tức',
      key: 'group_13',
      icon: 'down-square-o',
      url: '/news'
    },
    group_14: {
      title: 'Thông báo',
      key: 'group_14',
      icon: 'down-square-o',
      url: '/post-notification'
    },
    group_15: {
      title: 'Tài liệu',
      key: 'group_15',
      icon: 'down-square-o',
      url: '/document'
    },
  }
};

export default sideBarMenuConfig;