require('./bootstrap');
import React from 'react';
import { render } from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import {DashApp} from './dashApp';
ReactDOM.render(
    <DashApp/>,
    document.getElementById('root')
);