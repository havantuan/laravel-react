import React from 'react';
import {Tabs, Icon} from 'antd';
import PageTabsCardLayout from "../../../../layouts/PageTabsCardLayout";
import PostTable from "./PostTable";
import FeedTable from "./Feed/FeedTable";

export default class Post extends React.PureComponent {

  render() {
    return (
      <div style={{marginTop: 20}}>
        <PageTabsCardLayout>
          <Tabs type="card">
            {/*<Tabs.TabPane tab={<span><Icon type="table"/>Bài viết</span>} key={'1'}>*/}
              {/*<PostTable/>*/}
            {/*</Tabs.TabPane>*/}
            <Tabs.TabPane tab={<span><Icon type="printer"/>Cộng đồng</span>} key={'2'}>
              <FeedTable/>
            </Tabs.TabPane>
          </Tabs>
        </PageTabsCardLayout>
      </div>

    )
  }

}