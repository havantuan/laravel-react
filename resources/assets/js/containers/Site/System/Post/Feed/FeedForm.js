import React, {Component} from 'react';
import {Button, Col, Form, Input, Modal, Row, Spin, Select} from 'antd';
import {inject, observer} from "mobx-react";
import {Keys} from '../../../../../stores/index';
import Editor from "../../../../../components/Editor";
import basicStyle from "../../../../../config/basicStyle";
import UploadFileList from "../../../Common/Upload/UploadFileList";
import apiUrl from "../../../../../config/apiUrl";
import {replaceHtmlTag, trimPrefix} from "../../../../../helpers/utility";
const {Option} = Select;
const {TextArea} = Input;
const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 8},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 16},
  },
};
const tailFormItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 4},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 20},
  },
};
const FormItem = Form.Item;

@Form.create()
@inject(Keys.feedTable)
@observer
export default class PostForm extends Component {
    handleCancel = () => {
    this.props.form.resetFields();
    this.props.feedTable.onCancelModal();
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log('Received values of form: ', values);
      if (!err) {
        const prefixFormID = this.props.prefixFormID || '';
        let {ContentFormat, Content, PureContent} = trimPrefix(values, prefixFormID);
        let credentials = {
          ...values,
          ContentFormat: +ContentFormat,
          Content: +ContentFormat === 0 ? replaceHtmlTag(PureContent) : Content,
        };
        this.props.feedTable.onSubmitFormModal(credentials).then(() => {
          this.handleCancel();
        });
      }
    });
  };
  render() {
    const prefixFormID = this.props.prefixFormID || '';
    let {currentRow, isUpdateMode, isFetchingRowID} = this.props.feedTable;
    const {getFieldDecorator} = this.props.form;
    let loading = this.props.feedTable.isUpdating || this.props.feedTable.isCreating;
    let fetching = this.props.feedTable.isFetchingCurrentRow;
    return (
      <Modal
        title={`${isUpdateMode ? 'Cập nhật bài viết ' + (isFetchingRowID ? isFetchingRowID : '') : 'Thêm bài viết mới'}`}
        visible={this.props.feedTable.isShowModal}
        onOk={this.handleSubmit}
        width={'1000px'}
        onCancel={this.handleCancel}
        footer={[
          <Button key="cancel" onClick={this.handleCancel}>
            Hủy
          </Button>,
          <Button key="submit" type="primary" loading={loading} onClick={this.handleSubmit}>
            {isUpdateMode ? 'Cập nhật' : 'Tạo'}
          </Button>
        ]}
      >
        {fetching ? <Spin spinning/> :
          <Form onSubmit={this.handleSubmit}>
            {this.props.feedTable.groupType !== `Discussion` &&
            <FormItem
              {...tailFormItemLayout}
              label="Tiêu đề"
            >
              {getFieldDecorator(prefixFormID + 'Title', {
                rules: [
                  {required: false, message: 'Vui lòng nhập tiêu đề'}
                ],
                initialValue: isUpdateMode ? currentRow && currentRow.Title : null
              })(
                <Input
                  size="default"
                  placeholder="Nhập tiêu đề"
                />
              )}
            </FormItem>
            }

            <Row gutter={basicStyle.gutter}>
              <Col span={12}>
                <FormItem
                  {...formItemLayout}
                  label={'Upload ảnh'}
                >
                  {getFieldDecorator(prefixFormID + 'ImagesIDs', {
                    initialValue: isUpdateMode ? this.props.feedTable.getImageIDs(currentRow) : null
                  })(
                    <UploadFileList
                      listType={'picture'}
                      action={apiUrl.UPLOAD_IMAGE_URL}
                      placeholder={'Tải ảnh lên'}
                      fileList={this.props.feedTable.imageFileListToJS}
                      onValueChange={this.props.feedTable.onImagesChange}
                    />
                  )}
                </FormItem>
              </Col>

              <Col span={12}>
                <FormItem
                  {...formItemLayout}
                  label={'Upload tệp'}
                >
                  {getFieldDecorator(prefixFormID + 'DocumentsIDs', {
                    initialValue: isUpdateMode ? this.props.feedTable.getDocumentIDs(currentRow) : null
                  })(
                    <UploadFileList
                      showEditModal
                      action={apiUrl.UPLOAD_DOCUMENT_URL}
                      placeholder={'Tải tệp lên'}
                      fileList={this.props.feedTable.documentFileListToJS}
                      onValueChange={this.props.feedTable.onDocumentsChange}
                      onEditSuccess={this.props.feedTable.reload}
                    />
                  )}
                </FormItem>
              </Col>
            </Row>

            <FormItem
              {...tailFormItemLayout}
              label={'Định dạng văn bản'}
            >
              {getFieldDecorator(prefixFormID + 'ContentFormat', {
                initialValue: isUpdateMode ? currentRow && `${currentRow.ContentFormat}` : `1`
              })(
                <Select
                  onChange={this.props.feedTable.handleChangeFormat}
                >
                  <Option value='0'>Văn bản</Option>
                  <Option value='1'>Html</Option>
                </Select>
              )}
            </FormItem>
            {this.props.feedTable.formatContent === `1` ?
              <FormItem
                {...tailFormItemLayout}
                label={'Nội dung'}
              >
                {getFieldDecorator(prefixFormID + 'Content', {
                  initialValue: isUpdateMode ? currentRow && currentRow.Content : null
                })(
                  <Editor placeholder={'Nhập nội dung bài viết'}/>
                )}
              </FormItem> :
              <FormItem
                {...tailFormItemLayout}
                label={'Nội dung'}
              >
                {getFieldDecorator(prefixFormID + 'PureContent', {
                  initialValue: isUpdateMode ? currentRow && replaceHtmlTag(currentRow.Content) : null
                })(
                  <TextArea
                    rows={4}
                    placeholder={'Nhập nội dung bài viết'}/>
                )}
              </FormItem>
            }

          </Form>
        }
      </Modal>
    )
  }

}
