import React, {Component} from 'react';
import ObjectPath from 'object-path';
import {Button, Dropdown, Icon, Menu, Spin, Table, Popconfirm} from 'antd';
import ContentHolder from '../../../../components/utility/ContentHolder';
import AwardTableControl from "./AwardTableControl";
import PageHeaderLayout from '../../../../layouts/PageHeaderLayout';
import {inject, observer} from "mobx-react";
import {Keys} from '../../../../stores/index';
import TotalRecord from '../../../../components/TotalRecord/index';
import AwardForm from './AwardForm';
import UsersModal from "./UsersModal";
import Permission from "../../../../permissions/index";
@inject(Keys.awardTable)
@observer
export default class AwardTable extends Component {

  handleChange = (id, checked) => {
    this.props.awardTable.active(id, checked);
  };

  componentDidMount() {
    this.props.awardTable.reload();
  }

  componentWillUnmount() {
    this.props.awardTable.clear();
  }

  handleClickMenu = (id, {item, key, keyPath}) => {
    console.log("handleClickMenu", key);
    switch (key) {
      case 'edit':
        this.props.awardTable.showUpdateModal(id);
        break;
      case 'user':
        this.props.awardTable.showUsersModal(id);
        break;
      default:
        return;
    }
  };

  render() {
    let {dataSource, fetching} = this.props.awardTable;
    // let {activeData, deactiveData} = this.props;

    let columns = [{
      title: 'ID',
      dataIndex: 'ID',
      key: 'ID',
    }, {
      title: 'Icon giải thưởng',
      dataIndex: 'Icon',
      width: 160,
      key: 'Icon',
      render: (text, record, index) => <div>{ObjectPath.get(record, 'Icon.Url') && <img src={ObjectPath.get(record, "Icon.Url")} alt="" style={{maxWidth: '100%'}}/>}</div>,
    }, {
      title: 'Giải thưởng',
      dataIndex: 'Title',
      key: 'Title',
      render: (text, record, index) => <span><b>{ObjectPath.get(record, "Title")}</b></span>,
    }, {
      title: 'Mô tả',
      dataIndex: 'Description',
      key: 'Description',
      width: '30%',
      render: (text) => <span>{text}</span>
    }, {
      title: 'Thời gian',
      dataIndex: 'DateTime',
      key: 'DateTime',
      render: (text, record, index) => <span>{ObjectPath.get(record, "DateTime.Pretty")}</span>,
    }];

    columns.push({
      title: '',
      dataIndex: '',
      key: 'action',
      render: (text, record, index) => {
        const menu = (
          <Menu onClick={(e) => this.handleClickMenu(record.ID, e)}>
            {Permission.allowUpdateAward &&
            <Menu.Item key="edit">
              <Icon type="edit"/> Chỉnh sửa
            </Menu.Item>
            }
            {Permission.allowUpdateAward &&
            <Menu.Item key="delete">
              <Popconfirm
                title="Bạn có chắc chắn muốn xóa không?"
                onConfirm={() => {
                  this.props.awardTable.deleteAward(record.ID)
                }}
                okText="Có"
                cancelText="Không"
              >
                <Icon type="delete"/> Xóa bài viết
              </Popconfirm>
            </Menu.Item>
            }
            <Menu.Item key="user">
              <Icon type="user"/> Thành viên
            </Menu.Item>
          </Menu>
        );

        return (
          <Dropdown overlay={menu} trigger={['click']}>
            <Button
              icon="ellipsis"
              size="small"
            >
              Hành động
            </Button>
          </Dropdown>
        )
      }
    })
    return (
      <PageHeaderLayout title="Danh sách giải thưởng">
        <AwardForm/>
        <UsersModal/>
        <ContentHolder>
          <AwardTableControl/>
          <div style={{marginTop: 16}}>
            <TotalRecord total={this.props.awardTable.pagination.total} name={"giải thưởng"}/>

            <Spin spinning={fetching}>
              <Table
                dataSource={dataSource.slice()}
                columns={columns}
                rowKey={record => record.ID}
                pagination={this.props.awardTable.pagination}
                onChange={this.props.awardTable.handleTableChange}
              />
            </Spin>
          </div>
        </ContentHolder>
      </PageHeaderLayout>
    )
  }

}
