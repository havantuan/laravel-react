
import {Redirect, Route, withRouter} from "react-router-dom";
import React from "react";
export class RestrictedRoute extends React.Component {

  render() {
    const {component: Component, ...rest} = this.props;
      return <Route
        {...rest}
        render={props =>

             <Component {...props} />
        }
      />

  }
}